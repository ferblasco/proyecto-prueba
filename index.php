<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Juego de los Xinos</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="estilo.css" title="Color">
</head>
<style>
td {
	text-align: center;
}
</style>
<body>
	<?php

	$Jugador1 = rand(0,3);
	$Jugador2 = rand(0,3);
	$bocadillo1 = rand(0,6);
	$bocadillo2 = rand(0,6);n 

	$resultado = $Jugador1+$Jugador2;
	?>
		<table width="50%">
		<tr>
			<td colspan="2"><h1>JUEGO DE LOS CHINOS</h1></td>
			</tr>
		<tr>
			<td colspan="2"><h2>Actualice la p&aacute;gina para mostrar otra partida!.</h2></td>
			</tr>
					<tr>
						<td><h1>Jugador1</h1>
							<br>
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 117.97 107.3" width="200"><g stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
									<path fill="#ccc" stroke="#ccc" d="M23.8 6l78 5.4c8.4 0 15.2 6.7 15.2 15L112.2 74c0 8.3-5.4 14.4-13.8 14.4l-17 .3 4 17.6L60 89l-26 .8c-8.3 0-19-7.5-19-16L7 24c0-8.4 8.2-18 16.6-18z"/>
									<path fill="#fff" stroke="#000" d="M17.5 1l78 5.6c8.5 0 15.2 6.7 15.2 15L106 69.2c0 8.5-5.4 14.6-13.8 14.6l-17 .3 4 17.6L54 84.3 28 85c-8.6 0-19.5-7.5-19.5-16L1 19C1 10.4 9 1 17.5 1z"/>
									<text x="60" y="65" text-anchor="middle" style="font-size: 70px"><?= $bocadillo1 ?></text>
						</g>
					</svg>
				</td>
						<td><h1>Jugador2</h1>
							<br>
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 117.97 107.3" width="200"><g stroke-width="2" stroke-linejoin="round" stroke-linecap="round"> 
								<path fill="#ccc" stroke="#ccc" d="M94.2 6l-78 5.4C7.6 11.4 1 18 1 26.4L5.8 74c0 8.3 5.4 14.4 13.8 14.4l17 .3-4 17.6L57.8 89l26 .8c8.4 0 19.3-7.5 19.3-16l8-50c0-8.4-8-18-16.5-18z"/> 
								<path fill="#fff" stroke="#000" d="M100.5 1l-78 5.6C14 6.6 7 13.3 7 21.6L12 69c0 8.6 5.2 14.7 13.6 14.7l17 .3-4 17.4L64 84.2l26 .7c8.4 0 19.3-7.8 19.3-16l7.6-50c0-8.8-8-18-16.8-18z"/> 
								<text x="60" y="65" text-anchor="middle" style="font-size: 70px"><?= $bocadillo2 ?></text> 
						</g> 
					</svg>
				</td>
			</tr>
		<tr>
			<td><img src="img/chinos/chinos-<?= $Jugador1 ?>.svg" width="200px"></td><td><img src="img/chinos/chinos-<?= $Jugador2 ?>.svg" width="200px"></td>
			</tr>
		<tr>
		<td colspan="2">
	<?php
	if($bocadillo1 == $resultado && $bocadillo2 == $resultado){
		$resultado = "EMPATE!";
	}else{
		if($bocadillo1 != $resultado && $bocadillo2 != $resultado){
			$resultado = "NO ACERTO NADIE!";
		}else{
			if($bocadillo1 == $resultado && $bocadillo2 != $resultado){
				$resultado = "¡ACERTO EL JUEGDOR 1!";
			}else{
				$resultado = "¡ACERTO EL JUEGDOR 2!";
			}
		}
	}
	print("<h1>".$resultado."</h1>");
	?>
</td>
</tr>
</table>
</body>
</html>